import 'package:flutter/material.dart';

main() => runApp(MyApp());

/**
 * Widget:
 * 有状态的Widget:StatefulWidget 在运行过程中有一些状态（data）需要改变
 * 无状态的Widget:StatelessWidget 内容是确定没有状态（data）改变的
 */
class MyApp extends StatelessWidget{
  //build
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: GSJHomePage()
    );
  }
}

class GSJHomePage extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("第一个Flutter程序"),
      ),
      body: GSJContentBody(),
    );
  }
}

//Stateful 不能直接定义状态 -> 创建一个单独的类，这个类负责维护状态
class GSJContentBody extends StatefulWidget{
  @override
  State createState() {
    return GSJContentBodyState();
  }
}

class GSJContentBodyState extends State<GSJContentBody>{
  var flag = true;
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Checkbox(
              value: flag,
              onChanged: (value) {
                setState(() {
                  //这里才能监听到值的改变和界面改变
                  flag = value;
                });
                print(value);
              }
          ),
          Text("同意协议", style: TextStyle(fontSize: 20))
        ],
      ),
    );
  }
}