import 'package:flutter/material.dart';

class GSJContentBody extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Center(
        child:  Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Checkbox(
                value: true,
                onChanged: (value) => print(value)
            ),
            Text("同意协议", style: TextStyle(fontSize: 20),)
          ],
        )
    );
  }
}