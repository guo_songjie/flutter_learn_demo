class BaseResponse<T> {
  String reason;
  T result;
  String errorCode;

  BaseResponse({this.reason, this.result, this.errorCode});

  BaseResponse.fromJson(Map<String, dynamic> json) {
    reason = json['reason'];
    result = json['result'];
    errorCode = json['error_code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['reason'] = this.reason;
    data['result'] = this.result;
    data['error_code'] = this.errorCode;
    return data;
  }
}