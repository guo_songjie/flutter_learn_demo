class HttpConfig {
  static const String baseURL = "https://httpbin.org/get";
  static const int timeout = 5000;

  ///超时时间
  static const int CONNECT_TIMEOUT = 30000;
  static const int RECEIVE_TIMEOUT = 30000;

  //成功
  static const String HTTP_SUCCESS = "0";
}