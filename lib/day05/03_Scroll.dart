import 'package:flutter/material.dart';
import 'dart:math';

class GSJHomeContent extends StatefulWidget {
  @override
  _GSJHomeContentState createState() => _GSJHomeContentState();
}

class _GSJHomeContentState extends State<GSJHomeContent> {
  ScrollController _controller;
  bool _isShowTop = false;


  @override
  void initState() {
    _controller = ScrollController();

    _controller.addListener(() {
      var tempIsShowTop = _controller.offset >= 500;
      if(tempIsShowTop != _isShowTop)
        setState(() {
          _isShowTop = tempIsShowTop;
        });
    });
  }

  @override
  Widget build(BuildContext context) {
    return NotifationListenerDemo(isShowTop: _isShowTop);
  }
}

class NotifationListenerDemo extends StatelessWidget {
  const NotifationListenerDemo({
    Key key,
    @required bool isShowTop,
  }) : _isShowTop = isShowTop, super(key: key);

  final bool _isShowTop;

  @override
  Widget build(BuildContext context) {
    return NotificationListener(
      onNotification: (ScrollNotification notification){
        if(notification is ScrollStartNotification){
          print("开始滚动。。。");
        }else if(notification is ScrollEndNotification){
          print("结束滚动。。。");
        }else if(notification is ScrollUpdateNotification){
          print("正在滚动。。。，总滚动距离：${notification.metrics.maxScrollExtent},当前滚动的位置：${notification.metrics.pixels}");
        }
        return false;
      },
      child: ScrollControllDemo(controller: null, isShowTop: _isShowTop),
    );
  }
}

class ScrollControllDemo extends StatelessWidget {
  const ScrollControllDemo({
    Key key,
    @required ScrollController controller,
    @required bool isShowTop,
  }) : _controller = controller, _isShowTop = isShowTop, super(key: key);

  final ScrollController _controller;
  final bool _isShowTop;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        CustomScrollView(
          controller: _controller,
          slivers: <Widget>[
            SliverSafeArea(
              sliver: SliverGrid(
                  delegate: SliverChildBuilderDelegate(
                          (BuildContext ctx,int index){
                        return Container(color: Color.fromARGB(255, Random().nextInt(256),  Random().nextInt(256),  Random().nextInt(256)),child: Text("$index"),);
                      },
                      childCount: 10
                  ),
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 3,
                    mainAxisSpacing: 8,
                    crossAxisSpacing: 8,
                  )
              ),
            ),
            SliverGrid(
                delegate: SliverChildBuilderDelegate(
                        (BuildContext ctx,int index){
                      return Container(color: Color.fromARGB(255, Random().nextInt(256),  Random().nextInt(256),  Random().nextInt(256)),child: Text("${index + 10}"));
                    },
                    childCount: 10
                ),
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 3,
                  mainAxisSpacing: 8,
                  crossAxisSpacing: 8,
                )
            ),
            SliverFixedExtentList(
                itemExtent: 50,
                delegate: SliverChildBuilderDelegate(
                        (BuildContext context, int index){
                      return Container(color: Color.fromARGB(255, Random().nextInt(256),  Random().nextInt(256),  Random().nextInt(256)),child: Text("${index + 20}"));
                    },
                    childCount: 10
                )
            )
          ],
        ),
        Container(
          alignment: Alignment.bottomCenter,
          padding: EdgeInsets.only(bottom: 20),
          child: !_isShowTop ? null : FloatingActionButton(
              child: Icon(Icons.add),
              onPressed: (){
                _controller.animateTo(0, duration: Duration(milliseconds: 1000), curve: Curves.ease);
              }),
        )
      ],
    );
  }
}
