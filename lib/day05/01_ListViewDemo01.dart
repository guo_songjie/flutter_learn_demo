import 'package:flutter/material.dart';

class ListViewDemo extends StatelessWidget {
  const ListViewDemo({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      itemCount: 100,
      //默认预加载高度
      //cacheExtent: 200,
      itemBuilder:(BuildContext context,int index){
        return Container(
          height: 100,
          color: Colors.purple,
          child: ListTile(
            leading: Icon(Icons.people),//头
            trailing: Icon(Icons.delete),//尾
            title: Text("联系人:$index"),
            subtitle: Text("联系人电话号码：1881234567"),
          ),
        );
      },
      //分割线
      separatorBuilder: (BuildContext context,int index){
        return Divider(color: Colors.red,thickness: 2,);
      },

    );
  }
}

class ListViewDemo2 extends StatelessWidget {
  const ListViewDemo2({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      //准备展示数量，并不会立马创建
      itemCount: 100,
      //回调函数，懒加载
      itemBuilder: (BuildContext context,int index){
        return ListTile(
          leading: Icon(Icons.people),//头
          trailing: Icon(Icons.delete),//尾
          title: Text("联系人:$index"),
          subtitle: Text("联系人电话号码：1881234567"),
        );
      },
    );
  }
}

class ListViewDemo01 extends StatelessWidget {
  const ListViewDemo01({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      scrollDirection: Axis.vertical,
      //item范围
      itemExtent: 500,
      //倒序
      reverse: false,
      //1.匿名函数快速创建列表,明确数量且数量少
      children: List.generate(100, (index){
        //return Text("Hello World $index", style: TextStyle(fontSize: 20),);
        return Container(
          margin: EdgeInsets.only(bottom: 10,right: 10),
          color: Colors.red,
          child: ListTile(
            leading: Icon(Icons.people),//头
            trailing: Icon(Icons.delete),//尾
            title: Text("联系人:$index"),
            subtitle: Text("联系人电话号码：1881234567"),
          ),
        );
      }),
    );
  }
}