import 'package:flutter/material.dart';
import 'dart:math';
class GridViewDemo3 extends StatelessWidget {
  const GridViewDemo3({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 3,
            mainAxisSpacing: 8,
            crossAxisSpacing: 8
        ),
        itemBuilder: (BuildContext context, int index){
          return Container(
            width: 100,
            height: 100,
            color: Color.fromARGB(255, Random().nextInt(256), Random().nextInt(256), Random().nextInt(256)),
          );
        }
    );
  }
}

class GridViewDemo2 extends StatelessWidget {
  const GridViewDemo2({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GridView(
      //宽度最大值
        gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
          //最大宽度
            maxCrossAxisExtent: 100,
            //交叉轴间距
            crossAxisSpacing: 8,
            //主轴间距
            mainAxisSpacing: 8
        ),
        children: List.generate(100, (index){
          return Container(
              width: 100,
              height: 100,
              color: Color.fromARGB(255, Random().nextInt(256), Random().nextInt(256), Random().nextInt(256)),
              child: Center(child: Text("hello World"))
          );
        })
    );
  }
}

//GridView.count
class GridViewDemo1 extends StatelessWidget {
  const GridViewDemo1({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(8.0),
      child: GridView(
        //明确几个
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 3,
            //宽度/高度比例
            childAspectRatio: 1,
            //交叉轴间距
            crossAxisSpacing: 8,
            //主轴间距
            mainAxisSpacing: 8
        ),
        children: List.generate(100, (index){
          return Container(
              width: 100,
              height: 100,
              color: Color.fromARGB(255, Random().nextInt(256), Random().nextInt(256), Random().nextInt(256)),
              child: Center(child: Text("hello World"))
          );
        }),
      ),
    );
  }
}
