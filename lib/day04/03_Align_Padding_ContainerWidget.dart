import 'package:flutter/material.dart';

class ContainerDemo extends StatelessWidget {
  const ContainerDemo({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 500,
      height: 500,
      alignment: Alignment.topLeft,
      padding: EdgeInsets.all(8.0),
      margin: EdgeInsets.all(5.0),
      //transform: Matrix4.skewX(100),
      //圆角、背景图
      decoration: BoxDecoration(
          color: Colors.orange,
          borderRadius: BorderRadius.circular(4),
          boxShadow: [
            BoxShadow(
              color: Colors.purple,
              //偏移
              offset: Offset(3, 3),
              //延伸
              spreadRadius: 5,
              //模糊度
              //blurRadius: 10
            )
          ]
      ),
      child: Row(
        children: <Widget>[
          Container(width: 100, color: Colors.pink,child: Text("Hello World", style: TextStyle(fontSize: 20),)),
          Container(width: 180, margin: EdgeInsets.only(left: 2), color: Colors.red,child: Text("Hello World", style: TextStyle(fontSize: 20),)),
          Container(width: 100, color: Colors.pink,child: Text("Hello World", style: TextStyle(fontSize: 20),)),
        ],
      ),
    );
  }
}

class PaddingDemo extends StatelessWidget {
  const PaddingDemo({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text("Hello World" , style: TextStyle(fontSize: 20, backgroundColor: Colors.red),),
        ),
        Text("Hello World" , style: TextStyle(fontSize: 20, backgroundColor: Colors.red),),
        Text("Hello World" , style: TextStyle(fontSize: 20, backgroundColor: Colors.red),),
      ],
    );
  }
}

class AlignDemo extends StatelessWidget {
  const AlignDemo({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      //宽度、高度因子,根据child
      widthFactor: 10,
      heightFactor: 10,
      alignment: Alignment.bottomRight,
      child: Icon(
        Icons.pets,size: 50,
      ),
    );
  }
}