import 'package:flutter/material.dart';

class textFieldDemo extends StatelessWidget {

  final  userNameTextEditController = TextEditingController();
  final  passwordTextEditController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
        primaryColor: Color(0xFF00FFFF),
      ),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: <Widget>[
            TextField(
              controller: userNameTextEditController,
              //边框装饰
              decoration: InputDecoration(
                  labelText: "username",
                  icon: Icon(Icons.people),
                  hintText: "请输入用户名",
                  border: OutlineInputBorder(),
                  filled: true,
                  fillColor: Colors.red[100]
              ),
              onChanged: (value){
                print("onChange$value");
              },
              onSubmitted: (value){
                print("onSubmit$value");
              },
            ),
            SizedBox(height: 10,),
            TextField(
              controller: passwordTextEditController,
              decoration: InputDecoration(
                  labelText: "password",
                  icon: Icon(Icons.lock),
                  border: OutlineInputBorder()
              ),
            ),
            SizedBox(height: 10,),
            Container(
              width: double.infinity,
              height: 48,
              child: FlatButton(
                color: Colors.blue,
                child: Text("登录",style: TextStyle(fontSize: 20, color: Colors.white),),
                onPressed: (){
                  //获取用户名和密码
                  print("用户名：${userNameTextEditController.text}，密码：${passwordTextEditController.text}");
                  userNameTextEditController.text = "";
                  passwordTextEditController.text = "";
                  //收起键盘
                  FocusScope.of(context).unfocus();
                },
              ),
            ),
            OutlineButton(
              color: Colors.red,
              onPressed: (){},
              child: Text("Hello World"),
            )
          ],
        ),
      ),
    );
  }
}



