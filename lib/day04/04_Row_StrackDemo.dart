import 'package:flutter/material.dart';

class StackDemo extends StatefulWidget {
  const StackDemo({
    Key key,
  }) : super(key: key);

  @override
  _StackDemoState createState() => _StackDemoState();
}

class _StackDemoState extends State<StackDemo> {
  var iconButtonColor = Colors.indigo;

  @override
  Widget build(BuildContext context) {
    //默认包裹内容
    return Stack(
      children: <Widget>[
        Container(
          color: Colors.purple,
          width: 300,
          height: 300,
        ),
        Positioned(
            left: 20,
            top: 20,
            child: IconButton(
              icon: Icon(Icons.favorite, color: iconButtonColor),
              onPressed: () {
                setState(() {
                  print("点击了iconButton");
                  iconButtonColor = iconButtonColor == Colors.pink
                      ? Colors.indigo
                      : Colors.pink;
                });
              },
            )),
        Positioned(
          bottom: 20,
          right: 20,
          child: Container(
            color: Color.fromARGB(150, 250, 0, 0),
            child: Column(
              children: <Widget>[
                Text("你好啊，李银河",
                    style: TextStyle(fontSize: 20, color: Colors.white)),
                Text("你好啊，李银河",
                    style: TextStyle(fontSize: 20, color: Colors.white)),
                Text("你好啊，李银河",
                    style: TextStyle(fontSize: 20, color: Colors.white)),
              ],
            ),
          ),
        )
      ],
    );
  }
}

class RowDemo extends StatelessWidget {
  const RowDemo({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    /**
     * spaceBetween: 左右两边间距为0，其他元素之间平分间距
     * spaceAround: 左右两边间距是其他元素之间的间距的一般
     * spaceEvenly: 平分所有的间距
     */
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      crossAxisAlignment: CrossAxisAlignment.end,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        Expanded(flex: 1, child: Container(color: Colors.red, height: 60)),
        Container(color: Colors.blue, width: 80, height: 80),
        Container(color: Colors.green, width: 70, height: 70),
        Container(color: Colors.orange, height: 100),
        Expanded(
          flex: 2,
          child: Container(color: Colors.orange, height: 100),
        )
      ],
    );
  }
}