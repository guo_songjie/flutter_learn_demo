import 'package:flutter/material.dart';

class IconExtensionDemo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //Icon字体图标和图片图标区别
    //1.字体图标矢量图（放大的时候不会失真）
    //2.字体图标可以设置颜色
    //3.图标很多时，占据控件更小
    //return Icon(Icons.pets,size: 300, color: Colors.orange,);
    //return Icon(IconData(0xe91d, fontFamily: 'MaterialIcons'),size: 110, color: Colors.orange);
    //1.0xe91d -> unicode编码(\ue91d)
    //2.设置对应字体
    return Text(
      "\ue91d",
      style: TextStyle(
          fontSize: 100,
          color: Colors.orange,
          fontFamily: 'MaterialIcons'
      ),
    );
  }
}