import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

main() => runApp(MyApp());

class GSJCounterWidget extends InheritedWidget {
  //1.共享数据
  final int counter;

  //2.自定义构造方法
  GSJCounterWidget({this.counter, Widget child}): super(child: child);

  //3.获取最近的当前的InheritedWidget方法
  static GSJCounterWidget of(BuildContext context) {
    //沿着element树去找到最近的GSJContentElement,从Element中去除Widget对象
    return context.dependOnInheritedWidgetOfExactType();
  }

  //4.决定要不要回调State中的didChangeDependencies
  //如果返回ture,执行依赖当期的InheritedWidget的State中的didChangeDependencies
  @override
  bool updateShouldNotify(GSJCounterWidget oldWidget) {
    //更新后是否通知
    return oldWidget.counter != this.counter;
  }
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: GSJHomePage(),
    );
  }
}

class GSJHomePage extends StatefulWidget {
  @override
  _GSJHomePageState createState() => _GSJHomePageState();
}

class _GSJHomePageState extends State<GSJHomePage> {
  int _counter = 100;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Flutter Demo"), centerTitle: true,),
      body: GSJCounterWidget(
          counter: _counter,
          child: GSJHomeContent()
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: (){
          setState(() {
            _counter ++;
          });
        },
      ),
    );
  }
}

class GSJHomeContent extends StatefulWidget {

  @override
  _GSJHomeContentState createState() => _GSJHomeContentState();
}

class _GSJHomeContentState extends State<GSJHomeContent> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        GSJShowData01(),
        GSJShowData02(),
      ],
    );
  }
}

class GSJShowData01 extends StatefulWidget {
  @override
  _GSJShowData01State createState() => _GSJShowData01State();
}

class _GSJShowData01State extends State<GSJShowData01> {

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    print("执行了_didChangeDependencies");
  }

  @override
  Widget build(BuildContext context) {
    int counter = GSJCounterWidget.of(context).counter;
    return Card(
      color: Colors.red,
      child: Text("当前计数:$counter"),
    );
  }

}

class GSJShowData02 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    int counter = GSJCounterWidget.of(context).counter;
    return Container(
      color: Colors.blue,
      child: Text("当前计数：$counter"),
    );
  }
}

