import 'package:flutterapp/day07/model/user_info.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';

import 'counter_view_model.dart';
import 'user_view_model.dart';

List<SingleChildWidget> providers =  [
  ChangeNotifierProvider(create: (ctx) => GSJCounterViewModel(),),
  ChangeNotifierProvider(create: (ctx) => GSJUserViewModel(UserInfo("name",17,"")),)
];