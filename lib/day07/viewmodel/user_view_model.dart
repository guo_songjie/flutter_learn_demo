import 'package:flutter/cupertino.dart';
import 'package:flutterapp/day07/model/user_info.dart';

class GSJUserViewModel extends ChangeNotifier {
  UserInfo _userInfo;

  GSJUserViewModel(this._userInfo);

  UserInfo get userInfo => _userInfo;

  set userInfo(UserInfo value) {
    _userInfo = value;
  }
}