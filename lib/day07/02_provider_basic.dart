import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutterapp/day07/viewmodel/counter_view_model.dart';
import 'package:provider/provider.dart';

/**
 * 1.创建自己需要共享的数据
 * 2.在应用程序的顶层ChangeNotifierProvider
 * 3.在其他位置使用共享的数据
 * >Provider.of: 当前provider所在的Widget会被重新build
 * >consumer（相对推荐）: 当provider数据改变只会重新执行consumer的build
 * >selecter: 1.selector：对原有的数据类型进行转换，A->S；2.shouldRebuild:是否重新构建
 */

main() {
  runApp(
      ChangeNotifierProvider(
        create: (ctx) => GSJCounterViewModel(),
        child: MyApp(),
      )
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: GSJHomePage(),
    );
  }
}

class GSJHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text("Flutter Demo"), centerTitle: true,),
        body: GSJHomeContent(),
        floatingActionButton: Selector<GSJCounterViewModel, GSJCounterViewModel>(
          //可对原有的数据类型进行转换
          selector: (ctx, counterVM) => counterVM,
          //是否需要重新构建，builder是否重新构建
          shouldRebuild: (prev, next){
            return false;
          },
          builder: (ctx, counterVM, child){
            print("floatingActionButton的build方法被执行");
            return FloatingActionButton(
              child: child,
              onPressed: (){
                //赋值后执行notifyListeners（），通知监听者
                counterVM.counter += 1;
              },
            );
          },
          //优化不需要重构的widget
          child: Icon(Icons.add),
        )
    );
  }

  Consumer<GSJCounterViewModel> buildConsumer() {
    return Consumer<GSJCounterViewModel>(
      builder: (ctx, counterVM, child){
        print("floatingActionButton的build方法被执行");
        return FloatingActionButton(
          child: child,
          onPressed: (){
            //赋值后执行notifyListeners（），通知监听者
            counterVM.counter += 1;
          },
        );
      },
      //优化不需要重构的widget
      child: Icon(Icons.add),
    );
  }
}

//class GSJHomeContent extends StatefulWidget {
//  @override
//  _GSJHomeContentState createState() => _GSJHomeContentState();
//}
//
//class _GSJHomeContentState extends State<GSJHomeContent> {
//  @override
//  Widget build(BuildContext context) {
//    return Center(
//      child: Column(
//        mainAxisAlignment: MainAxisAlignment.center,
//        children: <Widget>[
//          GSJShowData01(),
//          GSJShowData02(),
//        ],
//      ),
//    );
//  }
//}

class GSJHomeContent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          GSJShowData01(),
          GSJShowData02(),
        ],
      ),
    );
  }
}

//Provider.of使用较少
class GSJShowData01 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    int counter = Provider.of<GSJCounterViewModel>(context).counter;
    print("buildData01");
    return Container(
      color: Colors.red,
      child: Text("当前计数：$counter"),
    );
  }
}

//consumer用的较多
class GSJShowData02 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    int counter = Provider.of<GSJCounterViewModel>(context).counter;
    print("buildData01");
    return Container(
      color: Colors.blue,
      child: Consumer<GSJCounterViewModel>(
        builder: (ctx,counterVM, child){
          print("GSJShowData02的consumer build方法被执行");
          return  Text("当前计数：$counter");
        },
      ),
    );
  }
}


