import 'package:flutter/material.dart'
    '';
Listener ListenerDemo() {
  return Listener(
    onPointerDown: (event) {
      print("指针按下");
      print("屏幕坐标${event.position}");
      print("当前坐标${event.localPosition}");
    },
    onPointerMove: (event) => print("指针移动"),
    onPointerUp: (event) => print("指针抬起"),
    onPointerCancel: (event) => print("指针取消(电话过来)"),
    child: Container(
        width: 100,
        height: 100,
        color: Colors.red,
        child: Text("Hello World")
    ),
  );
}

GestureDetector GestureDetectorDemo() {
  return GestureDetector(
    onTapDown: (details){
      print("手指按下");
      print("屏幕位置${details.globalPosition}");
      print("Widget位置${details.localPosition}");
    },
    onTapUp: (details){
      print("手指抬起");
    },
    onTapCancel: () {
      print("手指取消");
    },
    onTap: () {
      print("手指点击");
    },
    onDoubleTap: () {
      print("手指双击");
    },
    onLongPress: (){
      print("长按手势");
    },
    child: Container(
      width: 200,
      height: 200,
      color: Colors.orange,
    ),
  );
}