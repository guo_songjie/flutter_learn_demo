import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:event_bus/event_bus.dart';

//1.创建全局的EvenBus对象
//2.发出事件
//3.监听事件

final evenBus = EventBus();

main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: GSJHomePage(),
    );
  }
}

class GSJHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Flutter Demo"), centerTitle: true,),
      body: GSJHomeContent(),
    );
  }
}

class GSJHomeContent extends StatefulWidget {
  @override
  _GSJHomeContentState createState() => _GSJHomeContentState();
}

class _GSJHomeContentState extends State<GSJHomeContent> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          GSJText(),
          GSJBotton()
        ],
      )
    );
  }
}


class GSJBotton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      child: Text("按钮"),
      onPressed: (){
        //一般发送对象
        evenBus.fire("发射");
      },
    );
  }
}

class GSJText extends StatefulWidget {
  @override
  _GSJTextState createState() => _GSJTextState();
}

class _GSJTextState extends State<GSJText> {
  String _message = "Hello word";

  @override
  void initState() {
    super.initState();
    evenBus.on<String>().listen((event) {
      _message = event;
      print(event);
      setState(() {

      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Text(
      _message,
      style: TextStyle(fontSize: 30),
    );
  }
}

