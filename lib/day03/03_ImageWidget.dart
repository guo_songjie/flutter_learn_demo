import 'package:flutter/material.dart';

class ImageDemo02 extends StatelessWidget {
  const ImageDemo02({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Image(
      //1.在flutter项目中创建一个文件夹，存储图片
      //2.在pubspec.yaml进行配置
      //3.使用图片
      image: AssetImage("assets/images/xingkong.jpg"),
    );
  }
}

class ImageDemo01 extends StatelessWidget {
  final imageUrl = "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1598607900888&di=874306a56fba319ef35898ee07398e38&imgtype=0&src=http%3A%2F%2Fa2.att.hudong.com%2F36%2F48%2F19300001357258133412489354717.jpg";

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 300,
      child: ListView(
        children: <Widget>[
          //图片缓存，最多1000张
          ImageExtensionDemo(imageUrl: imageUrl),
          Image(
            color: Colors.green,
            //混入模式
            colorBlendMode: BlendMode.colorDodge,
            image: NetworkImage(imageUrl),
            width: 200,
            height: 200,
            fit: BoxFit.contain,
            //重复
            repeat: ImageRepeat.repeatY,
            //图片内部位置设置
            //alignment: Alignment.topCenter,
            //范围： -1 1
            alignment: Alignment(0,0),
          ),
          Image(
            image: NetworkImage(imageUrl),
            width: 200,
            height: 200,
            fit: BoxFit.cover,
          ),
          Image(
            image: NetworkImage(imageUrl),
            width: 200,
            height: 200,
            fit: BoxFit.fill,
          ),
          Image(
            image: NetworkImage(imageUrl),
            width: 200,
            height: 200,
            fit: BoxFit.fitHeight,
          ),
          Image(
            image: NetworkImage(imageUrl),
            width: 200,
            height: 200,
            fit: BoxFit.fitWidth,
          ),
        ],
      ),
    );
  }
}

class ImageExtensionDemo extends StatelessWidget {
  const ImageExtensionDemo({
    Key key,
    @required this.imageUrl,
  }) : super(key: key);

  final String imageUrl;

  @override
  Widget build(BuildContext context) {
    return FadeInImage(
      fadeOutDuration: Duration(seconds: 2),
      placeholder: AssetImage("assets/images/xingkong.jpg"),
      image: NetworkImage(imageUrl)
    );
  }
}