import 'package:flutter/material.dart';

class ButtonDemo extends StatelessWidget {
  final String str = "";
  const ButtonDemo({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: <Widget>[
        //1.RaisedButton
        RaisedButton(
            child: Text("RaisedButton"),
            textColor: Colors.purple,
            onPressed: () => print("RaisedButton Click")
        ),


        //2.FlatButton
        //buttonTheme 设置最小宽度
        ButtonTheme(
          minWidth: 30,
          height: 0,
          child: FlatButton(
            //设置内边距
            padding: EdgeInsets.all(0),
            //紧缩，去除button默认上下间距
            materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
            onPressed: () => print("FlatButton Click"),
            //去掉才会不显示
            child: Container(child: Text(str,),padding: EdgeInsets.all(0)),
            color: Colors.deepOrange,
          ),
        ),

        //3.outlineButton
        OutlineButton(
          child: Text("OutlineButton",),
          onPressed: () => print("OutlineButton Click"),
        ),

        //4.floatingActionButton 浮动
        FloatingActionButton(
            child: Icon(Icons.add),
            onPressed: () => print("FloatingActionButton Click")
        ),

        //5.自定义button:图标-文字-背景-圆角
        FlatButton(
          color: Colors.amberAccent,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8)
          ),
          onPressed:() {},
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Icon(Icons.favorite, color: Colors.red,),
              Text("喜欢作者"),
            ],
          ),
        )
      ],
    );
  }
}

