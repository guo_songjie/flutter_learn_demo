import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';



class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: GSJHomePage(),
    );
  }
}
class GSJHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: GSJHomeContent(),
    );
  }
}

class GSJHomeContent extends StatefulWidget {
  @override
  _GSJHomeContentState createState() => _GSJHomeContentState();
}

class _GSJHomeContentState extends State<GSJHomeContent> {
  @override
  Widget build(BuildContext context) {
    return Text.rich(
      TextSpan(
          text: "Hello World",
          style: TextStyle(fontSize: 50, color: Colors.red,),
          children: [
            TextSpan(text: "Hello World", style: TextStyle(fontSize: 20, color: Colors.green,)),
            TextSpan(text: "Hello World", style: TextStyle(fontSize: 20, color: Colors.yellow,)),
            WidgetSpan(child: Icon(Icons.favorite, color: Colors.red,)),
            TextSpan(text: "Hello World", style: TextStyle(fontSize: 20, color: Colors.pink,)),
          ]
      ),

    );
  }
}

class GSJDemo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Text.rich(
      TextSpan(
          text: " HelloWorld",
          style: TextStyle(fontSize: 50, color: Colors.red,),
          children: [
            TextSpan(text: "Hello000World", style: TextStyle(fontSize: 20, color: Colors.green,)),
            TextSpan(text: "HelloWorld", style: TextStyle(fontSize: 20, color: Colors.yellow,)),
            WidgetSpan(child: Icon(Icons.favorite, color: Colors.red,), alignment: PlaceholderAlignment.baseline, baseline: TextBaseline.alphabetic),
            TextSpan(text: "HelloWorld", style: TextStyle(fontSize: 20, color: Colors.pink,)),
          ]
      ),

    );
  }
}








