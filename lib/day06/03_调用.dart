import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutterapp/Utils/log_util.dart';
import 'package:flutterapp/http/http_manager.dart';

main() {
  runApp(MyApp());
}

void runFoo()  {
  HttpManager().getAsync<String>(
      url: "/get",
      tag: "Tag").then((value) =>  LogUtil.v("${value}4"));
  LogUtil.v("end");
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: GSJHomePage(),
    );
  }
}




class GSJHomePage extends StatefulWidget {

  //GlobalKey 访问某个widget信息
  final GlobalKey<_GSJHomeContentState> homeKey = GlobalKey();

  @override
  _GSJHomePageState createState() => _GSJHomePageState();
}

class _GSJHomePageState extends State<GSJHomePage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Flutter Demo"), centerTitle: true,),
      body:  GSJHomeContent(key: widget.homeKey),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.delete),
        onPressed: (){
          setState(() {
            print(widget.homeKey.currentState.message);
            print(widget.homeKey.currentState.widget.name);
            widget.homeKey.currentState.test();
          });
        },
      ),
    );
  }
}

class GSJHomeContent extends StatefulWidget {
  final String name = "coderwhy";
  GSJHomeContent({Key key}): super(key: key);

  @override
  _GSJHomeContentState createState() => _GSJHomeContentState();
}

class _GSJHomeContentState extends State<GSJHomeContent> {
  final String message = "123";
  @override
  Widget build(BuildContext context) {
    return Text(message);
  }

  test(){
    print("调用方法");
  }
}









