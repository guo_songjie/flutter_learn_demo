import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutterapp/Utils/log_util.dart';
import 'package:flutterapp/http/http_manager.dart';

main() {
  runApp(MyApp());
}

void runFoo()  {
  HttpManager().getAsync<String>(
      url: "/get",
      tag: "Tag").then((value) =>  LogUtil.v("${value}4"));
  LogUtil.v("end");
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: GSJHomePage(),
    );
  }
}

class GSJHomePage extends StatefulWidget {
  @override
  _GSJHomePageState createState() => _GSJHomePageState();
}

class _GSJHomePageState extends State<GSJHomePage> {
  List<String> names = ["111","2222","3333"];

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(title: Text("Flutter Demo"), centerTitle: true,),
      body:  ListView(
        children: names.map((item) {
          //uniquekey 唯一key
          return ListItemFul(item, key: ValueKey(item));
        }).toList(),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.delete),
        onPressed: (){
          setState(() {
            names.removeAt(0);
          });
        },
      ),
    );
  }
}

class ListItemLess extends StatelessWidget {
  final String name;
  final Color randomColor = Color.fromARGB(255, Random().nextInt(256), Random().nextInt(256), Random().nextInt(256));
  ListItemLess(this.name,{Key key}): super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Text(name),
      height: 80,
      color: randomColor,
    );
  }
}

class ListItemFul extends StatefulWidget {
  final String name;

  ListItemFul(this.name,{Key key}): super(key: key);

  @override
  _ListItemFulState createState() => _ListItemFulState();
}

class _ListItemFulState extends State<ListItemFul> {
  final Color randomColor = Color.fromARGB(255, Random().nextInt(256), Random().nextInt(256), Random().nextInt(256));

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Text(widget.name),
      height: 80,
      color: randomColor,
    );
  }
}








