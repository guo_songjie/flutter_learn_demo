import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutterapp/douban/pages/main/main.dart';

main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        //主题颜色
        primaryColor: Colors.green,
        splashColor: Colors.transparent,
        //默认高亮
        highlightColor: Colors.transparent,
      ),
      home: GSJMainPage(),
    );
  }
}







