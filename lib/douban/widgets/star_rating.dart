import 'package:flutter/material.dart';

class GSJStarRating extends StatefulWidget {
  final double rating;
  final double maxRating;
  final int count;
  final double size;
  final Color unSelectColor;
  final Color selectColor;
  final Widget unSelectImage;
  final Widget selectImage;

  GSJStarRating({
    @required this.rating,
    this.maxRating = 10,
    this.count = 5,
    this.size = 30,
    this.unSelectColor = Colors.grey,
    this.selectColor = Colors.yellow,
    Widget unSelectImage,
    Widget selectImage,
  }): unSelectImage = unSelectImage ?? Icon(Icons.star_border, color: unSelectColor, size: size,),
        selectImage = selectImage ?? Icon(Icons.star, color: selectColor, size: size,);

  @override
  _GSJStarRatingState createState() => _GSJStarRatingState();
}

class _GSJStarRatingState extends State<GSJStarRating> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Row(mainAxisSize: MainAxisSize.min, children: buildUnSelectStar(),),
        Row(mainAxisSize: MainAxisSize.min, children: buildSelectStar(),)
      ],
    );
  }

  List<Widget> buildUnSelectStar(){
    return List.generate(widget.count, (index) {
      return widget.unSelectImage;
    });
  }

  List<Widget> buildSelectStar(){
    if(widget.rating > widget.maxRating){
      return List.generate(widget.count, (index) {
        return widget.selectImage;
      });
    }
    //1.创建starts
    List<Widget> starts = [];
    //2.创建满填充starts
    int entireCount = (widget.count * widget.rating /widget.maxRating).floor();
    starts.addAll(List.generate(entireCount, (index) {
      return widget.selectImage;
    }));
    //3.创建部分填充starts
    double leftWidthScale = (widget.count * widget.rating /widget.maxRating) - entireCount;
    print(leftWidthScale);
    starts.add(ClipRect(
        clipper: GSJStarClipper(scale: leftWidthScale),
        child: widget.selectImage)
    );
    return starts;
  }
}

class GSJStarClipper extends CustomClipper<Rect> {
  double scale;

  GSJStarClipper({this.scale});

  @override
  Rect getClip(Size size) {
    return Rect.fromLTRB(0, 0, size.width*scale, size.height);
  }

  @override
  bool shouldReclip(GSJStarClipper oldClipper) {
    return oldClipper.scale != this.scale;
  }
}