import 'package:flutter/material.dart';
import 'package:flutterapp/douban/pages/main/initialize_items.dart';

typedef OnTap = void Function(int currenIndex);

class GSJFloatingBottomBar extends StatefulWidget {
  final List<BottomNavigationBarItem> item;
  final OnTap onTap;

  GSJFloatingBottomBar({
    @required this.item ,
    this.onTap
  });

  @override
  _GSJFloatingBottomBarState createState() => _GSJFloatingBottomBarState();
}

class _GSJFloatingBottomBarState extends State<GSJFloatingBottomBar> {
  int _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return ClipPath(
      clipper: BottomBarShadowClipper(),
      child: Stack(
        children: <Widget>[
          Container(
            height: 110,
            decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    //阴影
                      color: Color(0x06333333),
                      //模糊度
                      blurRadius: 10
                  )
                ]
            ),
            child: ClipPath(
              clipper: BottomBarClipper(),
              child: BottomNavigationBar(
                backgroundColor: Colors.white,
                iconSize: 25,
                selectedFontSize: 12,
                unselectedFontSize: 12,
                currentIndex: _currentIndex,
                //item超过四个会隐藏文字
                type: BottomNavigationBarType.fixed,
                items: items,
                onTap: (index){
                  _currentIndex = index;
                  widget.onTap(index);
                },
              ),
            ),
          ),
        ],

      ),
    );
  }
}

//贝塞尔裁剪阴影
class BottomBarClipper extends CustomClipper<Path>{

  @override
  Path getClip(Size size) {
    double clipHeight = 30;
    // 路径
    var path = Path();
    // 设置路径的开始点
    path.lineTo(0, size.height);
    path.lineTo(0, clipHeight);
    path.lineTo((size.width-80)/2, clipHeight);

    // 设置第一个曲线的样式
    var firstControlPoint = Offset(size.width / 2, -10);
    var firstEndPont = Offset(size.width / 2 + 40, clipHeight);
    // 把设置好的第一个样式添加到路径里面
    path.quadraticBezierTo(firstControlPoint.dx, firstControlPoint.dy, firstEndPont.dx, firstEndPont.dy);

    path.lineTo(size.width, clipHeight);
    path.lineTo(size.width, size.height);
    path.lineTo(0, size.height);
    // 返回路径
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return false;
  }
}


//贝塞尔裁剪bottomBar
class BottomBarShadowClipper extends CustomClipper<Path>{

  @override
  Path getClip(Size size) {
    double clipHeight = 28;
    // 路径
    var path = Path();
    // 设置路径的开始点
    path.lineTo(0, size.height);
    path.lineTo(0, clipHeight);
    path.lineTo((size.width-80)/2 - 2, clipHeight);

    // 设置第一个曲线的样式
    var firstControlPoint = Offset(size.width / 2, -12);
    var firstEndPont = Offset(size.width / 2 + 40 - 2, clipHeight - 2);
    // 把设置好的第一个样式添加到路径里面
    path.quadraticBezierTo(firstControlPoint.dx, firstControlPoint.dy, firstEndPont.dx, firstEndPont.dy);

    path.lineTo(size.width, clipHeight);
    path.lineTo(size.width, size.height);
    path.lineTo(0, size.height);
    // 返回路径
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return false;
  }
}
