import 'package:flutter/material.dart';
import 'home_content.dart';

class GSJHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("首页"),
        centerTitle: true,
      ),
      body: GSJHomeContent(),
    );
  }
}
