import 'package:flutter/material.dart';
import 'package:flutterapp/day03/01_TextRichWidget.dart';
import 'package:flutterapp/douban/service/home_request.dart';

class GSJHomeContent extends StatefulWidget {
  @override
  _GSJHomeContentState createState() => _GSJHomeContentState();
}

class _GSJHomeContentState extends State<GSJHomeContent> {

  @override
  void initState() {
    //HomeRequest.requestMovieList();
  }

  @override
  Widget build(BuildContext context) {
    return GSJDemo();
  }

}
