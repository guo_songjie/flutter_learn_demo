import 'package:flutter/material.dart';
import 'package:flutterapp/douban/pages/subject/subject_content.dart';

class GSJSubjectPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      appBar: AppBar(
        title: Text("书影音"),
        centerTitle: true,
      ),
      body: GSJSubjectContent(),
    );
  }
}
