import 'package:flutter/material.dart';
import 'package:flutterapp/douban/pages/group/group_content.dart';

class GSJGroupPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("小组"),
        centerTitle: true,
      ),
      body: GSJGroupContent(),
    );
  }
}
