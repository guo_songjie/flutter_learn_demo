import 'package:flutter/material.dart';
import 'package:flutterapp/douban/pages/group/group.dart';
import 'package:flutterapp/douban/pages/home/home.dart';
import 'package:flutterapp/douban/pages/mall/mall.dart';
import 'package:flutterapp/douban/pages/profile/profile.dart';
import 'package:flutterapp/douban/pages/subject/subject.dart';
import 'bottom_bar_item.dart';

List<BottomNavigationBarItem> items = [
  GSJBottomNavigationBarItem("home","首页"),
  GSJBottomNavigationBarItem("subject","书影音"),
  //GSJBottomNavigationBarItemWithFloatingButton("group"),
  GSJBottomNavigationBarItem("group","小组"),
  GSJBottomNavigationBarItem("mall","市集"),
  GSJBottomNavigationBarItemWithHint("profile","我的","5"),
];

List<Widget> pages = [
  GSJHomePage(),
  GSJSubjectPage(),
  GSJGroupPage(),
  GSJMallPage(),
  GSJProfilePage(),
];