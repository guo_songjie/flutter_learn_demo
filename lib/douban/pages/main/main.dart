import 'package:flutter/material.dart';
import 'initialize_items.dart';
class GSJMainPage extends StatefulWidget {
  @override
  _GSJMainPageState createState() => _GSJMainPageState();
}

class _GSJMainPageState extends State<GSJMainPage> {
  int _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: IndexedStack(
        index: _currentIndex,
        children: pages,
      ),
      bottomNavigationBar:buildBottomNavigationBar(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked, //融合底部工具栏
    );
  }

  //底部融合floatingActionButton
  Container buildFloatingActionButton() {
    return Container(
      width: 70,
      height: 70,
      padding: EdgeInsets.only(left: 10,right: 10),
      margin: EdgeInsets.only(bottom: 3),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(40),
        boxShadow: [
          BoxShadow(
            color: Colors.white,
            //偏移
            offset: Offset(0, -1),
            //延伸
            //spreadRadius: 5,
            //模糊度
            //blurRadius: 10
          )
        ]
      ),
      child: FloatingActionButton(
        onPressed: (){
        },
        tooltip: "长按提示",
        backgroundColor: Colors.yellow,
        child: Icon(Icons.add, color: Colors.white,),
      ),
    );
  }

  BottomNavigationBar buildBottomNavigationBar() {
    return BottomNavigationBar(
      backgroundColor: Colors.white,
      iconSize: 25,
      selectedFontSize: 12,
      unselectedFontSize: 12,
      currentIndex: _currentIndex,
      //item超过四个会隐藏文字
      type: BottomNavigationBarType.fixed,
      items: items,
      onTap: (index){
        setState(() {
          _currentIndex = index;
        });
      },);
  }

}
