import 'package:flutter/material.dart';
import 'package:flutterapp/Utils/log_util.dart';

class GSJBottomNavigationBarItem extends BottomNavigationBarItem {
  GSJBottomNavigationBarItem(iconName,title): super(
      title: Text(title,),
      icon: Image.asset("assets/images/tabbar/$iconName.png",width: 24, gaplessPlayback: true,),
      activeIcon: Image.asset("assets/images/tabbar/${iconName}_active.png",width: 24, gaplessPlayback: true,)
  );
}

class GSJBottomNavigationBarItemWithHint extends BottomNavigationBarItem {
  GSJBottomNavigationBarItemWithHint(iconName,title,hintText): super(
      title: Text(title,),
      icon: Container(
        child: Stack(
          alignment: Alignment.topRight,
          children: <Widget>[
            Image.asset("assets/images/tabbar/$iconName.png",width: 24, gaplessPlayback: true,),
            Container(
                alignment: Alignment.center,
                width: 10,
                height: 10,
                decoration: BoxDecoration(
                    color: Colors.red,
                    borderRadius: BorderRadius.circular(5)
                ),
                //color: Colors.red,
                child: Text(hintText, style: TextStyle(fontSize: 8, color: Colors.white),)
            ),
          ],
        ),
      ),
      activeIcon: Stack(
        alignment: Alignment.topRight,
        children: <Widget>[
          Image.asset("assets/images/tabbar/${iconName}_active.png",width: 24, gaplessPlayback: true),
          Container(
              alignment: Alignment.center,
              width: 10,
              height: 10,
              decoration: BoxDecoration(
                  color: Colors.red,
                  borderRadius: BorderRadius.circular(5)
              ),
              //color: Colors.red,
              child: Text(hintText, style: TextStyle(fontSize: 8, color: Colors.white),)
          ),
        ],
      )
  );
}

class GSJBottomNavigationBarItemWithFloatingButton extends BottomNavigationBarItem {
  GSJBottomNavigationBarItemWithFloatingButton(iconName): super(
    title: Text("",),
      icon: Container(
        width: 60,
        height: 60,
        child: FloatingActionButton(
          child: Image.asset("assets/images/tabbar/$iconName.png",width: 24,),
          onPressed: (){
            LogUtil.v("点击了floatingActionButton");
          },
        ),
      ),
      //activeIcon: Image.asset("assets/images/tabbar/${iconName}_active.png",width: 32,)
  );
}

