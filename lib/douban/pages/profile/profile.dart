import 'package:flutter/material.dart';
import 'package:flutterapp/douban/pages/profile/profile_content.dart';
class GSJProfilePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("我的"),
        centerTitle: true,
      ),
      body: GSJProfileContent(),
    );
  }
}
