import 'package:flutter/material.dart';

main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: GSJHomePage(),
    );
  }
}

class GSJHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Center(
            child: Text("商品列表"),
          )
      ),
      body:GSJHomeContent() ,
    );
  }
}

class GSJHomeContent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        GSJHomeProductItem("title1","desc1","https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1598509086885&di=ce73721756088295032c5f6ce48b2150&imgtype=0&src=http%3A%2F%2Fimg1.gtimg.com%2Frushidao%2Fpics%2Fhv1%2F20%2F108%2F1744%2F113431160.jpg"),
        SizedBox(height: 6,),
        GSJHomeProductItem("title2","desc2","https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1598509086885&di=ce73721756088295032c5f6ce48b2150&imgtype=0&src=http%3A%2F%2Fimg1.gtimg.com%2Frushidao%2Fpics%2Fhv1%2F20%2F108%2F1744%2F113431160.jpg"),
        SizedBox(height: 6,),
        GSJHomeProductItem("title3","desc3","https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1598509086885&di=ce73721756088295032c5f6ce48b2150&imgtype=0&src=http%3A%2F%2Fimg1.gtimg.com%2Frushidao%2Fpics%2Fhv1%2F20%2F108%2F1744%2F113431160.jpg")
      ],
    );
  }
}

class GSJHomeProductItem extends StatelessWidget {
  final String title;
  final String desc;
  final String imageUrl;

  GSJHomeProductItem(this.title,this.desc,this.imageUrl);

  @override
  Widget build(BuildContext context) {
    final style1 = TextStyle(fontSize: 25, color: Colors.orange);
    final style2 = TextStyle(fontSize: 20, color: Colors.green);
    return Container(
      padding: EdgeInsets.all(8),
      decoration: BoxDecoration(
          border: Border.all(
              width: 5, //设置边框宽度
              color: Colors.pink //设置边框颜色
          )
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Text(title,style: style1),
            ],
          ),
          SizedBox(height: 8,),
          Text(desc, style: style2),
          SizedBox(height: 8,),
          Image.network(imageUrl)
        ],
      ),
    );
  }
}



