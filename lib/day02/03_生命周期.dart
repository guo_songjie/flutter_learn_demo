import 'package:flutter/material.dart';

main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: GSJHomePage(),
    );
  }
}

class GSJHomePage extends StatelessWidget {
  final List<Widget> widgets = [GSJHomeContent()];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: Center(
              child: Text("商品列表"),
            )
        ),
        body:Column(
          children: <Widget>[
            GSJHomeContent()
          ],
        )
    );
  }
}

//StatelessWidget的生命周期
//class GSJHomeContent extends StatelessWidget {
//  final String message;
//
//  GSJHomeContent(this.message){
//    print("构造函数被调用");
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    print("调用build方法");
//    return Text(message);
//  }
//}

//StatefulWidget的生命周期
class GSJHomeContent extends StatefulWidget {
  GSJHomeContent(){
    print("1.调用GSJHomeContent的constructor方法");
  }
  @override
  _GSJHomeContentState createState() {
    print("2.调用GSJHomeContent的createState方法");
    return _GSJHomeContentState();
  }
}

class _GSJHomeContentState extends State<GSJHomeContent>  {
  AnimationController _controller;
  int _counter = 0;

  _GSJHomeContentState(){
    print("3.调用_GSJHomeContentState的constructor方法");
  }

  @override
  void initState() {
    //必须调用super
    super.initState();
    print("4.调用_GSJHomeContentState的createState方法");
  }

  @override
  Widget build(BuildContext context) {
    print("5.调用_GSJHomeContentState的build方法");
    return Column(
      children: <Widget>[
        RaisedButton(
            child: Icon(Icons.add),
            onPressed: (){
              setState(() {
                _counter++;
              });
            }
        ),
        Text(_counter.toString())
      ],
    );
  }

  @override
  void dispose() {
    print("6.调用_GSJHomeContentState的dispose方法");
    _controller.dispose();
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    print("7.调用_GSJHomeContentState的didChangeDependencies方法");
  }

  @override
  void didUpdateWidget(GSJHomeContent oldWidget) {
    super.didUpdateWidget(oldWidget);
    print("8.调用_GSJHomeContentState的didUpdateWidget方法");
  }
}





