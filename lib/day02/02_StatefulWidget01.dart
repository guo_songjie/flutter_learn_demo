import 'package:flutter/material.dart';

main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: GSJHomePage(),
    );
  }
}

class GSJHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Center(
            child: Text("商品列表"),
          )
      ),
      body:GSJHomeContent("你好啊") ,
    );
  }
}

//Widget不加_:暴露给别人使用
class GSJHomeContent extends StatefulWidget {
  final String message;

  GSJHomeContent(this.message);

  @override
  State createState() {
    return _GSJHomeContentState();
  }
}

//State加_:状态这个类只是给Widget使用
class _GSJHomeContentState extends State<GSJHomeContent> {
  var _counter = 0;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          _getButtons(),
          Text("当前计数$_counter", style: TextStyle(fontSize: 25),),
          Text("传递的信息:${widget.message}", style: TextStyle(fontSize: 25),)
        ],
      ),
    );
  }

  Widget _getButtons(){
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        RaisedButton(
            child: Text("+", style: TextStyle(fontSize: 25,color: Colors.white)),
            color: Colors.pink,
            onPressed: () {
              setState(() {
                _counter++;
              });
            }
        ),
        RaisedButton(
            child: Text("-", style: TextStyle(fontSize: 25,color: Colors.white)),
            color: Colors.purple,
            onPressed: () {
              setState(() {
                _counter--;
              });
            }
        )
      ],
    );
  }
}


