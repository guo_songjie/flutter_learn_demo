///日志输出
class LogUtil {
  static const String _TAG_DEFAULT = "###gsj###";

  ///是否 debug
  static bool debug = true; //是否是debug模式,true: log v 不输出.

  static String tagDefault = _TAG_DEFAULT;

  static void init({bool isDebug = false, String tag = _TAG_DEFAULT}) {
    debug = isDebug;
    tag = tag;
  }

  static void e(Object object, {String tag,}) {
    _printLog(tag, '  e  ', object);
  }

  static void v(Object object, {String tag,StackTrace current}) {
    if (debug) {
     
      _printLog(tag, '  v  ', object);
    }
  }


  static void _printLog(String tag, String stag, Object object,{ StackTrace current}) {
    StringBuffer sb = StringBuffer();
    if(current != null){
      GSJCustomTrace programInfo = GSJCustomTrace(current);
      sb.write("所在文件: ${programInfo.fileName}, 所在行: ${programInfo.lineNumber}");
    }
    sb.write((tag == null || tag.isEmpty) ? tagDefault : tag);
    sb.write(stag);
    sb.write(object);
    print(sb.toString());
  }
}

class GSJCustomTrace {
  final StackTrace _trace;

  String fileName;
  int lineNumber;
  int columnNumber;

  GSJCustomTrace(this._trace) {
    _parseTrace();
  }

  void _parseTrace() {
    var traceString = this._trace.toString().split("\n")[0];
    var indexOfFileName = traceString.indexOf(RegExp(r'[A-Za-z_]+.dart'));
    var fileInfo = traceString.substring(indexOfFileName);
    var listOfInfos = fileInfo.split(":");
    this.fileName = listOfInfos[0];
    this.lineNumber = int.parse(listOfInfos[1]);
    var columnStr = listOfInfos[2];
    columnStr = columnStr.replaceFirst(")", "");
    this.columnNumber = int.parse(columnStr);
  }
}