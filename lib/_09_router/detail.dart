import 'package:flutter/material.dart';

class GSJDetailPage extends StatelessWidget {
  static const String routerName = "/detail";
  final String _message;

  GSJDetailPage(this._message);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        //当返回为true时候，可以自动返回（flutter帮助我们执行返回操作）
        //当返回false,自行写返回代码
        _backHome(context);
        return Future.value(false);
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text("Flutter Demo"),
          centerTitle: true,
          //返回按钮监听
//          leading: IconButton(
//            icon: Icon(Icons.arrow_back_ios),
//            onPressed: () => _backHome(context)
//          ),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              RaisedButton(
                child: Text("回到首页"),
                onPressed: () => _backHome(context),
              ),
              Text(_message),
            ],
          ),
        ),
      ),
    );
  }

  void _backHome(BuildContext context){
    Navigator.of(context).pop("返回数据");
  }
}
