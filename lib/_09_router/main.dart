import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutterapp/_09_router/about.dart';
import 'package:flutterapp/_09_router/detail.dart';
import 'package:flutterapp/_09_router/router/router.dart';

main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: GSJRouter.routers,
      initialRoute: GSJRouter.initialRouter,
      onGenerateRoute: GSJRouter.generateRouter,
      onUnknownRoute: GSJRouter.unknownRouter,
    );
  }
}

class GSJHomePage extends StatelessWidget {
 static const String routerName = "/";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Flutter Demo"), centerTitle: true,),
      body: GSJHomeContent(),
    );
  }
}

class GSJHomeContent extends StatefulWidget {
  String _homeMMeaage = "默认数据";
  @override
  _GSJHomeContentState createState() => _GSJHomeContentState();
}

class _GSJHomeContentState extends State<GSJHomeContent> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(widget._homeMMeaage, style: TextStyle(fontSize: 20),),
          RaisedButton(
            child: Text("跳转详情",style: TextStyle(fontSize: 20),),
            onPressed: () => _jumpToDetail(context),
          ),
          RaisedButton(
            child: Text("跳转关于",style: TextStyle(fontSize: 20),),
            onPressed: () => _jumpToAbout(context),
          ),
          RaisedButton(
            child: Text("跳转详情2",style: TextStyle(fontSize: 20),),
            onPressed: () => _jumpToDetail2(context),
          ),
          RaisedButton(
            child: Text("跳转设置",style: TextStyle(fontSize: 20),),
            onPressed: () => _jumpToSettigs(context),
          ),
        ],
      )
    );
  }

  void _jumpToDetail(BuildContext context) {
    //1.普通的跳转方式
    //传递参数：通过构造器直接传递
    Future result = Navigator.of(context).push(MaterialPageRoute(builder: (ctx) {
      return GSJDetailPage("传递参数");
    }));
    result.then((value) {
      setState(() {
        widget._homeMMeaage = value;
      });
    });
  }

  void _jumpToAbout(BuildContext context) {
    Navigator.of(context).pushNamed(GSJAboutPage.routerName, arguments: "a home message");
  }

  void _jumpToDetail2(BuildContext context) {
    Navigator.of(context).pushNamed(GSJDetailPage.routerName, arguments: "a home message");
  }

  void _jumpToSettigs(BuildContext context) {
    Navigator.of(context).pushNamed("/cc");
  }
}


