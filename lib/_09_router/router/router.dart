import 'package:flutter/material.dart';

import '../about.dart';
import '../detail.dart';
import '../main.dart';
import '../unKnow.dart';

class GSJRouter  {
  static final Map<String, WidgetBuilder> routers =  {
    GSJHomePage.routerName: (ctx) => GSJHomePage(),
    GSJAboutPage.routerName: (ctx) => GSJAboutPage(),
  };

  static final String initialRouter = GSJHomePage.routerName;

  static final RouteFactory generateRouter = (settings) {
    if(settings.name == GSJDetailPage.routerName){
      return MaterialPageRoute(
          builder: (ctx) {
            return GSJDetailPage("传递参数2");
          }
      );
    }
    return null;
  };

  static final RouteFactory unknownRouter = (settings) {
    return MaterialPageRoute(
      builder: (ctx) {
        return  GSJUnKnowPage();
      },
    );
  };
}